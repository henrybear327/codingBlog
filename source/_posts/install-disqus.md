---
title: Install Disqus on Hexo
tags:
  - Hexo
  - Disqus
date: 2016-08-18 19:26:30
---


# [Install Disqus on Hexo](https://github.com/iissnan/hexo-theme-next/wiki/设置多说-DISQUS)

1. Sign up for an account [on Disqus](https://disqus.com/profile/signup/). After signing up, you will be given a `disqus shortname` for your website.
2. Add the `disqus shortname` to your **theme's** `_config.yml`. To be specific, update `disqus_shortname: your-disqus-shortname`

You should be good to go!

# Usage

The comments should be automatically added to every post that you have published. To disable the commenting feature on any of the post, add `comments: false` at the post's front-matter part of the `.md` file
