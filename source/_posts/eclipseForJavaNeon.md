---
title: Eclipse for Java
categories: Java
tags:
  - Java
  - Eclipse
date: 2016-10-01 23:51:33
---


# [Installation](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/marsr)

Google search for `Eclipse IDE for Java Developers`.

# [Auto-completion](http://stackoverflow.com/questions/6202223/auto-code-completion-on-eclipse)

For some reason, this feature isn't activated by default. Hmm....

Go to `Eclipse -> preference -> Java -> Editor -> Content Assist`, add `.abcdefghijklmnopqrstuvwxyz` to auto activation triggers for Java.
