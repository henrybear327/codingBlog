---
title: Install Latex On OSX Sierra
categories: Latex
tags:
  - Latex
date: 2016-10-01 23:51:33
---


# Installation

## Use `brew cask`

The easiest way!

`brew cask install mactex texstudio`

## Download packages:

The hard way!

1. [MacTex](http://www.tug.org/mactex/downloading.html)
2. [TexStudio](http://texstudio.sourceforge.net)

Install Mactex package simply by running the `.pkg` file and follow its instructions.

## Add font

1. Download fonts from Google fonts.
2. Double click on the `.ttf` file, or use [`font book`](https://support.apple.com/en-us/HT201749) to install the font
